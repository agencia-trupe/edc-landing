$(document).ready(function() {
  var $navigationLinks = $('header nav a');
  var $sections = $($('section').get().reverse());

  $navigationLinks.click(function(event) {
    event.preventDefault();

    var id = $(this).attr('href');

    $('html, body').animate({
      scrollTop: $('section' + id).offset().top - $('header').height() + 1
    }, 'slow');
  });

  $('.logo').click(function(event) {
    event.preventDefault();

    $('html, body').animate({
      scrollTop: 0
    }, 'slow');
  });

  var sectionIdTonavigationLink = {};
  $sections.each(function() {
    var id = $(this).attr('id');
    sectionIdTonavigationLink[id] = $('header nav a[href=\\#' + id + ']');
  });

  function throttle(fn, interval) {
    var lastCall, timeoutId;
    return function () {
      var now = new Date().getTime();
      if (lastCall && now < (lastCall + interval) ) {
        clearTimeout(timeoutId);
        timeoutId = setTimeout(function () {
          lastCall = now;
          fn.call();
        }, interval - (now - lastCall) );
      } else {
        lastCall = now;
        fn.call();
      }
    };
  }

  function highlightNavigation() {
    var scrollPosition = $(window).scrollTop();

    if (scrollPosition + $(window).height() >= $(document).height() - 100) {
      return $navigationLinks
        .removeClass('active')
        .last()
        .addClass('active');
    }

    $sections.each(function() {
      var currentSection = $(this);
      var sectionTop = currentSection.offset().top;

      if (scrollPosition + $('header').height() >= sectionTop) {
        var id = currentSection.attr('id');
        var $navigationLink = sectionIdTonavigationLink[id];

        if (!$navigationLink.hasClass('active')) {
          $navigationLinks.removeClass('active');
          $navigationLink.addClass('active');
        }

        return false;
      }
    });
  }

  $(window)
    .scroll(throttle(highlightNavigation, 100))
    .trigger('scroll');
});
